package delegate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.qiangzi.workflow.engine.bpmn.delegate.DelegateExecution;
import com.qiangzi.workflow.engine.bpmn.delegate.JavaDelegate;

public class SetParamsActivity implements JavaDelegate {

	public static final Logger LOGGER = LoggerFactory.getLogger(SetParamsActivity.class);

	@Override
	public void execute(DelegateExecution execution) throws Exception {

		// 模拟设置变量1
		execution.setVariable("securityStatus", "abcffg", String.class);
		String obj = (String) execution.getVariable("securityStatus");
		LOGGER.debug("value is -> " + obj);

		// 模拟设置变量2
		execution.setVariable("test", new SetParamsActivity(), SetParamsActivity.class);
		SetParamsActivity activity = (SetParamsActivity) execution.getVariable("test");
		LOGGER.debug("value is -> " + activity);

	}

}
