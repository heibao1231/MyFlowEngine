package com.qiangzi.workflow.engine.service.impl;

import com.qiangzi.workflow.engine.bpmn.base.ProcessDefinition;
import com.qiangzi.workflow.engine.bpmn.base.ProcessInstance;
import com.qiangzi.workflow.engine.service.RepositoryService;

public class MemoryRepositoryService implements RepositoryService {

	public ProcessInstance deploy(ProcessDefinition topology) throws Exception {

		return topology.deploy();

	}

}
