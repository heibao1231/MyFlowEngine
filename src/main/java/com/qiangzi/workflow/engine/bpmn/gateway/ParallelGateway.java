package com.qiangzi.workflow.engine.bpmn.gateway;

import org.dom4j.Element;

public class ParallelGateway extends Gateway {

	public void parse(Element element) throws Exception {
		super.parse(element);
	}

	public void deploy() {
		super.deploy();
	}

	/**
	 * 并行网关:前面已经确认过都没有表达式,所以直接调用父类的invoke即可
	 * 
	 * @throws Exception
	 */
	// @Override
	// public void invoke(ProcessInstance instance) throws Exception {
	// super.invoke(instance);
	// }

}
